import {createTheme} from '@mui/material';
import {frFR} from '@mui/material/locale';

export const theme = createTheme({
    palette: {
        primary: {main: '#2f2b99', light: '#6555cb', dark: '#00056a', contrastText: '#ffffff'},
        secondary: {main: '#6280fb', light: '#9aafff', dark: '#1b1f52', contrastText: '#ffffff'},
        text: {primary: '#21267d', secondary: '#6669a6', disabled: '#8c8ebc'},
        action: {
            active: 'rgba(0, 5, 106, 0.54)',
            hover: 'rgba(27, 31, 82, 0.04)',
            selected: 'rgba(98, 128, 251, 0.54)',
            disabled: 'rgba(27, 31, 82, 0.26)',
            disabledBackground: 'rgba(27, 31, 82, 0.15)',
            focus: 'rgba(27, 31, 82, 0.12)'
        },
        error: {main: '#f45341', dark: '#ba1718', light: '#ff866d', contrastText: '#ffffff'},
        info: {main: '#ac85ff', dark: '#7957cb', light: '#e0b5ff', contrastText: '#ffffff'},
        warning: {main: '#ff9359', dark: '#c7642d', light: '#ffc487', contrastText: '#ffffff'},
        success: {main: '#8ff4c1', dark: '#5cc191', light: '#c3fff4', contrastText: '#ffffff'},
    },
    typography: {
        fontSize: 14,
        fontFamily: ['Wow'].join(','),
        h1: {
            fontSize: '36px', fontWeight: 'bold', textAlign: 'center',
            '@media(min-width: 1366px)': {
                fontSize: '72px',
            }
        },
        h2: {fontSize: '36px', fontWeight: 'bold', textAlign: 'center', color: '#9BA0A5'},
        h3: {fontSize: '29px', fontWeight: 'bold', textAlign: 'center', lineHeight: '30px', color: 'white'},
        subtitle1: {fontWeight: 'normal', fontSize: '16px'},
        subtitle2: {
            fontWeight: 'book', fontSize: '16px',
            '@media(min-width: 1366px)': {
                fontSize: '24px',
            }
        },
        body1: {fontWeight: 'bold', fontSize: '24px'},
        body2: {fontWeight: 'lighter', fontSize: '14px'},
        button: {fontWeight: 'bold', fontSize: '24px'},
        caption: {fontWeight: 'normal', fontSize: '12px'},
        overline: {fontWeight: 'normal', fontSize: '12px'},
    },
}, frFR);