import React from 'react';
import {ContainerSection} from "../styled-utils/ContainerSection";
import {Box, styled, Typography, useMediaQuery} from "@mui/material";
import Hero1 from '../../media/rocket.png';
import ScrollArrow from '../../media/downarrow.svg';

const BackgroundContainer = styled('div')(() => ({
    backgroundImage: `url(${Hero1})`,
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
}))
const Container = styled(ContainerSection)(() => ({
    height: '100vh',
    flexDirection: 'column',
    justifyContent: 'space-between', alignItems: 'center',
}));
const Titles = styled(Box)(() => ({
    color: 'white',
    textAlign: 'center',

    '@media(min-width: 1366px)': {
        padding: '120px 180px',
    }
}))

const Hero: React.FC = () => {
    const isDesktop = useMediaQuery('(min-width: 1366px)')

    return (
        <BackgroundContainer>
            <Container>
                <p>1</p>

                <Titles>
                    <Typography variant='h1'>ExtraWowrdinary</Typography>
                    <Typography variant='subtitle2'>An almost ordinary journey - {!isDesktop && <br/>}04.23.2021</Typography>
                </Titles>

                <img alt='scrollArrow' src={ScrollArrow} />
            </Container>
        </BackgroundContainer>
    )
}

export default Hero