import React from 'react'
import {WowServices} from "../../utils/types";
import {Box, styled, Typography} from "@mui/material";

const Service = styled(Box)(() => ({
    maxWidth: '100px',
    display: 'flex', flexDirection: 'column',
    alignItems: 'center', justifyContent: 'center',
    fontVariantLigatures: 'discretionary-ligatures',
    textAlign: 'center',
    cursor: 'pointer',
    '&:hover': {
        opacity: '100%',
    },
    transition: '.5s ease-in-out'
}))
const Icon = styled(Typography)(() => ({
    fontSize: '50px', overflow: 'hidden',
}))
const ServiceName = styled(Typography)(() => ({
    //
}))

type Props = {
    icon: string,
    title: string,
    service: WowServices,
    selected: boolean,
    handleSelectService: (service: WowServices) => void,
}

const ServiceSelector: React.FC<Props> = ({icon, title, service, selected, handleSelectService}) => {
    return (
        <Service onClick={() => handleSelectService(service)} sx={{opacity: selected ? '100%' : '25%'}}>
            <Icon>{icon}</Icon>
            <ServiceName variant='subtitle2'>{title}</ServiceName>
        </Service>
    )
}

export default ServiceSelector;