import React from 'react';
import {WowServices} from "../../utils/types";
import {styled, Typography} from "@mui/material";
import ScrollArrow from '../../media/downarrow.svg';

const DescriptionImage = styled('img')(() => ({
    maxWidth: '',
}))
const RightArrow = styled('img')(() => ({
    transform: 'rotate(-90deg)',
    marginRight: '25px',
}))
const CTAMeet = styled('a')(() => ({
    display: 'flex',
    alignItems: 'center',
    marginTop: '42px',
    cursor: 'pointer',
}));

type Props = {
    service: WowServices
    image: string,
    description: string,
}

const ServiceItem: React.FC<Props> = ({service, image, description}) => {
    return (
        <>
            <DescriptionImage alt={service} src={image}/>
            <Typography variant='body1' sx={{marginTop: '25px'}}>{description}</Typography>
            <CTAMeet>
                <RightArrow alt='scrollArrow' src={ScrollArrow}/>
                <Typography variant='body1'>Let's meet</Typography>
            </CTAMeet>
        </>
    )
}

export default ServiceItem;