import React from 'react';
import {Box, styled, Typography} from "@mui/material";
import {ContainerSection} from "../styled-utils/ContainerSection";
import {WowServices} from "../../utils/types";
import ServiceSelector from "./ServiceSelector";
import Postits from '../../media/postits2x.png';
import Palette from '../../media/palette2x.png';
import Code from '../../media/code2x.png';
import Data from '../../media/data2x.png';
import ServiceItem from "./ServiceItem";

const Container = styled(ContainerSection)(() => ({
    maxHeight: '100%',
    flexDirection: 'column',
    background: '#202124', color: 'white',
}))
const StyledTitle = styled(Typography)(() => ({
    color: 'white',
    fontFamily: 'Wow',
    fontFeatureSettings: 'ss10',
    marginBottom: '42px',
}))
const Supreme = styled('span')(() => ({
    color: 'white',
    fontFamily: 'Supreme',
}))
const ServicesContainer = styled(Box)(() => ({
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '25px',
}))

const Services: React.FC = () => {
    const [currentService, setCurrentService] = React.useState<WowServices>('strategy');
    const handleSelectService = (service: WowServices) => {
        setCurrentService(service)
    }

    return (
        <Container>
            <StyledTitle variant='h2'>
                <Supreme>What</Supreme> is Wow doing <Supreme>?</Supreme>
            </StyledTitle>

            <ServicesContainer>
                <ServiceSelector icon='STRATEGY'
                                 title='Strategy & Consulting'
                                 service='strategy'
                                 selected={currentService === 'strategy'}
                                 handleSelectService={handleSelectService}
                />

                <ServiceSelector icon='DESIGN'
                                 title='Design & Production'
                                 service='design'
                                 selected={currentService === 'design'}
                                 handleSelectService={handleSelectService}
                />

                <ServiceSelector icon='CODE'
                                 title='Technology & Innovation'
                                 service='tech'
                                 selected={currentService === 'tech'}
                                 handleSelectService={handleSelectService}
                />

                <ServiceSelector icon='ADS'
                                 title='Advertising & Data'
                                 service='ads'
                                 selected={currentService === 'ads'}
                                 handleSelectService={handleSelectService}
                />
            </ServicesContainer>

            {{
                'strategy': <ServiceItem
                    service='strategy'
                    image={Postits}
                    description='With more of 150 projects from the beginning, Wow can help you develop a strategy and expand it during all of our collaboration.'
                />,
                'design': <ServiceItem
                    service='design'
                    image={Palette}
                    description='From design to websites, not to mention videos, photographs and so on, Wow is the perfect company to create all the design of your brand and decline it everywhere.'
                />,
                'tech': <ServiceItem
                    service='tech'
                    image={Code}
                    description='Care deeply about your web presence ? Wow has professionals in different fields relative to web to create a website, fully adapted to you and your needs.'
                />,
                'ads': <ServiceItem
                    service='ads'
                    image={Data}
                    description='Your brand is on social medias ? Wow accompany you on Facebook, LinkedIn, Instagram, TikTok to attract people to your brand.'
                />,
            }[currentService]}

        </Container>
    )
}

export default Services;