import React from 'react';
import {styled, Typography} from "@mui/material";
import {ContainerSection} from "../styled-utils/ContainerSection";
import {ButtonMore} from "../styled-utils/ButtonMore";
import Team from '../../media/team.png';

const Container = styled(ContainerSection)(() => ({
    flexDirection: 'column',
    alignContent: 'flex-start', justifyContent: 'flex-start',
    padding: '11px 36px 105px 36px',

    '@media(min-width: 1366px)': {
        width: '40%',
    }
}))
const Text = styled(Typography)(() => ({
    fontSize: '46px',
    fontFamily: 'Supreme',
    marginBottom: '22px',
}))
const TeamImage = styled('img')(() => ({
    width: '100%', height: '622px',
    objectFit: 'cover', objectPosition: 'center center',
}))

const Wow: React.FC = () => {
    return (
        <>
            <Container>
                <Text>
                    Whiz kids both geeks and salesmen, your mind and hand power, who kick your butt and rock your world taking care of everything and anything.
                </Text>

                <ButtonMore text='Who are the wowrriors ?' justify='flex-start'/>
            </Container>

            <TeamImage alt='Wow team' src={Team}/>
        </>
    )
}

export default Wow;