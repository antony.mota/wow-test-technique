import React from 'react';
import {Box, styled, Typography} from "@mui/material";
import {ContainerSection} from "../styled-utils/ContainerSection";
import Fineo from '../../media/logo-fineo.svg';
import Unilabs from '../../media/logo-unilabs.svg';
import Safehost from '../../media/logo-safehost.svg';
import Astrocast from '../../media/logo-astrocast.png';
import Harley from '../../media/logo-harley.svg';
import AGC from '../../media/logo-agc.svg';
import Cartier from '../../media/logo-cartier.png';
import Smurfs from '../../media/logo-smurfs.svg';

const Container = styled(ContainerSection)(() => ({
    backgroundColor: '#F9F9F9',
    flexDirection: 'column',
    padding: '86px 28px',
    alignItems: 'center', justifyContent: 'center',
}))
const LogoGrid = styled(Box)(() => ({
    width: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    marginTop: '52px',

    '@media(min-width: 1366px)': {
        gridTemplateColumns: '1fr 1fr 1fr 1fr',
    }
}))
const LogoItem = styled(Box)(() => ({
    height: '92px',
    display: 'flex',
    alignItems: 'center', justifyContent: 'center',
    borderBottom: '1px solid #B6B6B6',
    borderRight: '1px solid #B6B6B6',
    opacity: '75%',
    '&:nth-child(even)': {borderRight: 'none'},
    '&:nth-last-child(-n+2)': {borderBottom: 'none'},
    '& img': {
        display: 'block',
        maxHeight: '55px', maxWidth: '100px',
        height: 'auto', width: 'auto',
    },

    '@media(min-width: 1366px)': {
        '&:nth-child(even)': {borderRight: '1px solid #B6B6B6'},
        '&:nth-last-child(5)': {borderRight: 'none'},
        '&:nth-last-child(-n+4)': {borderBottom: 'none'},
        '&:last-child': {borderRight: 'none'},
        '& img': {
            display: 'block',
            maxHeight: '138px', maxWidth: '168px',
            height: 'auto', width: 'auto',
        },
    }
}))

const Clients: React.FC = () => {
    return (
        <Container>
            <Typography variant='h2'>Some of our clients</Typography>

            <LogoGrid>
                <LogoItem><img alt='Fineo' src={Fineo}/></LogoItem>
                <LogoItem><img alt='Unilabs' src={Unilabs}/></LogoItem>
                <LogoItem><img alt='Safehost' src={Safehost}/></LogoItem>
                <LogoItem><img alt='Astrocast' src={Astrocast}/></LogoItem>
                <LogoItem><img alt='Harley' src={Harley}/></LogoItem>
                <LogoItem><img alt='AGC' src={AGC}/></LogoItem>
                <LogoItem><img alt='Smurfs' src={Smurfs}/></LogoItem>
                <LogoItem><img alt='Cartier' src={Cartier}/></LogoItem>
            </LogoGrid>
        </Container>
    )
}

export default Clients;