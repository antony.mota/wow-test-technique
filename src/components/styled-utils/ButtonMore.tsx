import React from 'react';
import {styled} from "@mui/material";
import {Button} from "@mui/material";
import SideArrow from '../../media/sidearrow.svg'

const StyledButton = styled(Button)(() => ({
    display: 'flex',
    alignItems: 'center',
    color: 'black',
    fontFamily: 'Supreme',
    textTransform: 'none',
}))
const StyledArrow = styled('img')(() => ({
    width: '50px', height: '100%',
    paddingTop: '2px',
}))

type Props = {
    text: string,
    justify?: string,
}

export const ButtonMore: React.FC<Props> = ({text, justify}) => {
    return (
        <StyledButton variant='text' sx={{justifyContent: justify ? justify : 'center'}}>
            <StyledArrow alt='sideArrow' src={SideArrow}/>
            {text}
        </StyledButton>
    )
}