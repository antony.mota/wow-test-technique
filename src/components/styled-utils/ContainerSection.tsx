import {Box, styled} from "@mui/material";

export const ContainerSection = styled(Box)(() => ({
    maxWidth: '100vw',
    maxHeight: '100vh',
    display: 'flex',
    padding: '86px 20px',

    '@media(min-width: 1366px)': {
        padding: '120px 180px',
    }
}))