import React from 'react';
import {Box, styled, Typography, useMediaQuery} from "@mui/material";
import {ContainerSection} from "../styled-utils/ContainerSection";
import {blogPosts} from "../../utils/blogPosts";
import Play from '../../media/play.svg';
import {ButtonMore} from "../styled-utils/ButtonMore";

const Container = styled(ContainerSection)(() => ({
    display: 'grid',
    gridTemplateRows: '3fr',
    gridRowGap: '20px',
    alignContent: 'center', justifyContent: 'center',
}))
const FeaturedPost = styled('div')(() => ({
    maxHeight: '313px', maxWidth: '313px',
    height: '313px', width: '313px',
    backgroundRepeat: 'no-repeat',
    borderRadius: '12px',
    color: 'white',
}))
const FeaturedContent = styled(Box)(() => ({
    height: '100%',
    display: 'flex', flexDirection: 'column',
    alignItems: 'flex-start', justifyContent: 'flex-end',
    textAlign: 'left',
    padding: '0 18px 20px 18px',
    '& p:first-child': {
        fontSize: '20px',
    },
    '& p:last-child': {
        color: '#9BA0A5',
        fontSize: '16px',
    },
}))
const PlayIcon = styled('img')(() => ({
    height: '33px', width: '33px',
    margin: 'auto'
}))
const Post = styled(Box)(() => ({
    display: 'flex',
    alignContent: 'center',
}))
const PostThumbnail = styled('div')(() => ({
    maxHeight: '72px', maxWidth: '72px',
    height: '68px', width: '68px',
    display: 'flex',
    alignContent: 'center', justifyContent: 'center',
    backgroundRepeat: 'no-repeat',
    borderRadius: '12px',
}))
const PostContent = styled(Box)(() => ({
    display: 'flex', flexDirection: 'column',
    alignContent: 'flex-start', justifyContent: 'center',
    marginLeft: '20px',
    fontSize: '18px',
    '& p:first-child': {
        fontSize: '18px',
    },
    '& p:last-child': {
        color: '#9BA0A5',
        fontSize: '16px',
    },
}))

const Wlog: React.FC = () => {
    const isDesktop = useMediaQuery('(min-width: 1366px)')

    return (
        <Container>
            <Typography variant='h2' sx={{marginBottom: '42px'}}>Wlog</Typography>

            {blogPosts.map((blogPost, index) => {
                const isFeatured = blogPost.isFeatured

                return (
                    <>{isFeatured ?
                        <FeaturedPost sx={{backgroundImage: `url(${blogPost.thumbnail})`}}>

                            <FeaturedContent>
                                <Typography>{blogPost.title}</Typography>
                                <Typography>{blogPost.author}</Typography>
                            </FeaturedContent>
                        </FeaturedPost>
                        :
                        (index <= 3 || isDesktop) && <Post>
                            <PostThumbnail sx={{backgroundImage: `url(${blogPost.thumbnail})`}}>
                                <PlayIcon alt={'play icon'} src={Play}/>
                            </PostThumbnail>

                            <PostContent>
                                <Typography>{blogPost.title}</Typography>
                                <Typography>{blogPost.author}</Typography>
                            </PostContent>
                        </Post>
                    }</>
                )
            })}

            <Box sx={{marginTop: '36px'}}>
                <ButtonMore text='Click for exclusive content'/>
            </Box>
        </Container>
    )
}

export default Wlog;