import React from 'react';
import {ContainerSection} from "../styled-utils/ContainerSection";
import {styled, Typography} from "@mui/material";
import {ButtonMore} from "../styled-utils/ButtonMore";
import Carousel from "./Carousel";

const Container = styled(ContainerSection)(() => ({
    flexDirection: 'column',
    justifyContent: 'flex-start', alignItems: 'center',
    textAlign: 'center',
}));

const Works: React.FC = () => {
    return (
        <Container id='works'>
            <Typography variant='h2' sx={{marginBottom: '40px'}}>Featured Work</Typography>

            <Carousel/>

            <ButtonMore text='Discover all works'/>
        </Container>
    )
}

export default Works;