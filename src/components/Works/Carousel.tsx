import React from 'react';
import Satellite from '../../media/satellite.png';
import Medical from '../../media/medical.png';
import Teaching from '../../media/teaching.png';
import Astrocast from '../../media/astrocast.svg';
import Unilabs from '../../media/unilabs.svg';
import Crea from '../../media/crea.svg';
import Esprimed from '../../media/esprimed.png';
import {Box, styled, Typography} from "@mui/material";

const CarouselGrid = styled(Box)(() => ({
    width: '100%',
    display: 'grid',
    gridTemplateColumns: 'repeat(4, 313px)',
    gridTemplateRows: '1',
    gridAutoFlow: 'column',
    overflowX: 'scroll', overflowY: 'hidden',
    scrollbarWidth: 'thin',
    marginBottom: '52px',
}))
const CarouselItem = styled(Box)(() => ({
    display: 'inline-block',
    maxHeight: '570px',
    marginRight: '20px',
}))
const Image = styled('img')(() => ({
    height: '100%', width: '100%',
    linearGradient: ''
}))
const Logo = styled('img')(() => ({
    position: 'relative',
    bottom: '220px',
}))
const TitleContainer = styled(Box)(() => ({
    position: 'relative',
    bottom: '175px',
    display: 'flex',
    alignItems: 'center', justifyContent: 'center',
    padding: '0 15px',
}))
const Title = styled(Typography)(() => ({
    fontFeatureSettings: 'ss10',
}))
const Flip = styled('span')(() => ({
    display: 'inline-block',
    transform: 'scale(-1, 1)',
}))

const Carousel: React.FC = () => {
    return (
        <CarouselGrid>
            <CarouselItem>
                <Image src={Satellite}/>
                <Logo src={Astrocast}/>
                <TitleContainer>
                        <Title variant='h3'>“ First Swiss nano satellite in space <Flip>“</Flip></Title>
                </TitleContainer>
            </CarouselItem>

            <CarouselItem>
                <Image src={Medical}/>
                <Logo src={Unilabs}/>
                <TitleContainer>
                    <Title variant='h3'>“ The largest suppliers of medical diagnostics in Europe <Flip>“</Flip></Title>
                </TitleContainer>
            </CarouselItem>

            <CarouselItem>
                <Image src={Teaching}/>
                <Logo src={Crea}/>
                <TitleContainer>
                    <Title variant='h3'>“ Teaching today for tomorrow’s work <Flip>“</Flip></Title>
                </TitleContainer>
            </CarouselItem>

            <CarouselItem>
                <Image src={Medical}/>
                <Logo src={Esprimed}/>
                <TitleContainer>
                    <Title variant='h3'>“ Pioneers in medical imaging consulting <Flip>“</Flip></Title>
                </TitleContainer>
            </CarouselItem>
        </CarouselGrid>
    )
}

export default Carousel;