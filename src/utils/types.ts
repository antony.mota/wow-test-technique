export type WowServices = 'strategy' | 'design' | 'tech' | 'ads'

export type BlogPost = {
    title: string,
    author: string,
    thumbnail: string,
    isFeatured: boolean,
}