import {BlogPost} from "./types";
import Digital from '../media/digitaltransformation.png';
import Phone from '../media/phone.png';
import Neontech from '../media/neontech.png';
import Laptop from '../media/laptop.png';
import Forest from '../media/forest.png';
import Numbers from '../media/numbers.png';
import Coded from '../media/coded.png';

export const blogPosts: Array<BlogPost> = [
    {
        title: 'Digital transformation : How to understand it ?',
        author: 'Paul Boag',
        thumbnail: Digital,
        isFeatured: true,
    },
    {
        title: 'Luxury management : What…',
        author: 'Jean-Noël Kapferer',
        thumbnail: Phone,
        isFeatured: false,
    },
    {
        title: 'The power of the blockhain',
        author: 'Adrien Treccani',
        thumbnail: Neontech,
        isFeatured: false,
    },
    {
        title: 'Social network in Europe',
        author: 'Cedric Rainotte',
        thumbnail: Laptop,
        isFeatured: false,
    },
    {
        title: 'Take action for the planet',
        author: 'Bertrand Piccard',
        thumbnail: Forest,
        isFeatured: false,
    },
    {
        title: 'Numerical analysis in…',
        author: 'Brian Solis',
        thumbnail: Numbers,
        isFeatured: false,
    },
    {
        title: 'Build a global leader…',
        author: 'Vincent Pignon',
        thumbnail: Coded,
        isFeatured: false,
    },
]