import React from 'react';
import './App.css';
import {theme} from "./themeOptions";
import {ThemeProvider} from "@mui/material";
import {ContainerPage} from "./components/styled-utils/ContainerPage";
import Hero from "./components/Hero/Hero";
import Works from "./components/Works/Works";
import Services from "./components/Services/Services";
import Wlog from "./components/Wlog/Wlog";
import Clients from "./components/Clients/Clients";
import Wow from "./components/Wow/Wow";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <ContainerPage className="wow">
                <header></header>

                <Hero/>

                <Works/>

                <Services/>

                <Wlog/>

                <Clients/>

                <Wow/>
            </ContainerPage>
        </ThemeProvider>
    );
}

export default App;
